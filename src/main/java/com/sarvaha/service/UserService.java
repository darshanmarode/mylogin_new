package com.sarvaha.service;
import com.sarvaha.model.User;
 
public interface UserService {
     User findById(int id);
     User findByUserName(String username);   
}