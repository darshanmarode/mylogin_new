package com.sarvaha.dao;
 
import com.sarvaha.model.User;;
 
public interface UserDao
{
    User findById(int id);
    User findByUsername(String username);
}
