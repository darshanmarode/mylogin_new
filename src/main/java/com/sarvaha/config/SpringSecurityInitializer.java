package com.sarvaha.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer 
{
//This would simply only register the springSecurityFilterChain Filter 
	//for every URL in your application.
//After that we would ensure that SecurityConfig was loaded in our existing ApplicationInitializer. 
}

