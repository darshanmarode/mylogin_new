<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script src="javascript/validation.js"></script>


</head>
<body onload='document.loginForm.username.focus();' style="background: cadetblue;">
	<div class="container" >
		<div class="well" style="width: 220px;margin-top: 15%;margin-left: auto;margin-right: auto;">
			<fieldset>
				<legend>User Login Form</legend>
				<c:url var="loginUrl" value="/login" />
				<form name="loginForm" action="${loginUrl}" onsubmit="return inputcheck()" method="post" >
					
					<div class="row">
						<div class="col-sm-4" style="">
							<input type="text" name="username" value=""
								placeholder="Enter Username">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4" style="margin-top: 10px;">
							<input type="password" name="password" value=""
								placeholder="Enter Password">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4" style="margin-top: 10px; margin-left: 60px;">
							<input type="submit" value="Login">
						</div>

					</div>

					<c:if test="${param.error != null}">
						<div class="alert alert-danger">
							<p id=demo>Invalid username and password.</p>
						</div>
					</c:if>
					<c:if test="${param.logout != null}">
						<div class="alert alert-success">
							<p>You have been logged out successfully.</p>
						</div>
					</c:if>
					<%-- <div style="color:#ff6666;">
					<p id=demo value=""> <%=request.getParameter("error")%></p>
					</div> --%>
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
				</form>
			</fieldset>
		</div>
	</div>
</body>
</html>
