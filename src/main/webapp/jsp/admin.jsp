<%@page session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Welcome Admin</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="javascript/validation.js" type="text/javascript"></script>

</head>
<body>

<%-- 	<h1>Title : ${title}</h1>
	<h1>Message : ${message}</h1>
 --%>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<p></p>
			<p>
				Welcome <strong>${user}</strong>
			</p>
			<ul class="nav navbar-nav">
				<li><a onclick="send('Admin');" href="#">Admin</a></li>
				<li><a onclick="send('Setting');" href="#">Setting</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="<c:url value="/logout" />"> Logout</a></li>
			</ul>
		</div>
	</nav>

	<div class="container">
		<p id=datatag></p>
	</div>

</body>
</html>